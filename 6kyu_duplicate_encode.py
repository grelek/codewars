
# https://www.codewars.com/kata/54b42f9314d9229fd6000d9c
def duplicate_encode(word):

    word = word.lower()
    new_word = ''
    for sign in word:
        if word.count(sign) > 1:
            new_word += ')'
        else:
            new_word += '('

    return new_word




if __name__ == '__main__':

    print(duplicate_encode('Tomek'))
    print(duplicate_encode('TomekT'))