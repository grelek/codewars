
# https://www.codewars.com/kata/5279f6fe5ab7f447890006a7
def pick_peaks(a):
    pos_t = []
    peaks_t = []

    if a != []:
        actual_el = a[1]
        pos = 1

        up = 0
        slide = 0

        for i in range(1, len(a)):

            if a[i] > a[i - 1]:
                up = 1
                slide = 0

            elif a[i] < a[i - 1]:
                if up == 1:
                    up = 0

                    if slide == 1:
                        slide = 0
                        pos_t.append(i_tmp)
                        peaks_t.append(a_tmp)
                    else:
                        pos_t.append(i - 1)
                        peaks_t.append(a[i - 1])

            elif a[i] == a[i - 1]:
                if slide == 0 and up == 1:
                    slide = 1
                    i_tmp = i - 1
                    a_tmp = a[i - 1]

    result = {}
    result["pos"] = pos_t
    result["peaks"] = peaks_t

    return result


if __name__ == '__main__':

    c = [1, 1, 2, 1, 1, 2, 3, 2, 2, 2, 1, 1, 1]

    print(pick_peaks(c))

