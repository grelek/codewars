
# https://www.codewars.com/kata/52742f58faf5485cae000b9a
def format_duration(seconds):

    if seconds == 0:
        return 'now'

    time = seconds

    second = 1
    minute = 60 * second
    hour = 60 * minute
    day = 24 * hour
    year = 365 * day

    list_of_times = [year, day, hour, minute]

    my_seconds = 0
    my_minutes = 0
    my_hours = 0
    my_days = 0
    my_years = 0

    list_of_my_times = [my_years, my_days, my_hours, my_minutes, my_seconds]
    list_of_labels = ['year', 'day', 'hour', 'minute', 'second']

    t = 0
    while time >= minute:

        if time >= list_of_times[t]:
            list_of_my_times[t] += 1
            time -= list_of_times[t]
            continue
        else:
            t += 1

    list_of_my_times[4] = time


    result = ''
    number_of_my_times = 5 - list_of_my_times.count(0)

    for i in range(0, 5):
        if list_of_my_times[i] > 0:
            result += f'{list_of_my_times[i]} {list_of_labels[i]}'

            if list_of_my_times[i] > 1:
                result += 's'

            if number_of_my_times > 2:
                result += ', '
            elif number_of_my_times == 2:
                result += ' and '

            number_of_my_times -= 1

    return result



if __name__ == '__main__':

    print(format_duration(120))
    print(format_duration(1))
    print(format_duration(2))
    print(format_duration(120))
    print(format_duration(2143))
    print(format_duration(435436))
    print(format_duration(543265))
    print(format_duration(432654266))
