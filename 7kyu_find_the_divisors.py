
# https://www.codewars.com/kata/544aed4c4a30184e960010f4
def divisors(integer):

    arr = []

    for i in range(2,int(integer/2)+1):
        if integer % i == 0:
            arr.append(i)

    if not arr:
        arr = str(integer) + ' is prime'

    return arr


if __name__ == '__main__':

    print(divisors(15))
    print(divisors(2))
    print(divisors(13))
    print(divisors(12))