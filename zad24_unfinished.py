import math

def permutations(string):

    len_of_string = len(string)
    string_as_list = list(string)
    result_tab = []
    silnia = math.factorial(len_of_string-1)

    memory = string_as_list.copy()
    while len(list(set(result_tab))) < silnia:
        for x in range(len_of_string):

            for y in range(len_of_string):

                tmp = memory[x]
                memory[x] = memory[y]
                memory[y] = tmp

                result = ''.join(memory)
                result_tab.append(result)

    return list(set(result_tab))

    #'aabb', 'abab', 'abba', 'baab', 'baba', 'bbaa'

if __name__ == '__main__':

    print(sorted(permutations('aabb')))
    print(permutations('aabb'))
    print(permutations('abc'))