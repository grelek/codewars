
# https://www.codewars.com/kata/534e01fbbb17187c7e0000c6
def spiralize(size):

    #szukanie zaleznosci
    # size -> ilosc jedynek -> kolejne roznice
    #1 -> 1 2
    #
    # 2 -> 3 4
    # 3 -> 7 4
    #
    # 4 -> 11 6
    # 5 -> 17 6
    #
    # 6 -> 23 8
    # 7 -> 31 8
    #
    # 8 -> 39 10
    # 9 -> 49
    #
    delta = 2
    counter = 2
    snake_len = 1
    if size != 1:
        for i in range(2,size+1):
            snake_len += delta
            if counter == 2:
                counter = 0
                delta += 2
            counter += 1

    #tworzenie matrycy NxN
    a = []
    for i in range(size):
        row = []
        for j in range(size):
            row.append(0)
        a.append(row)

    #parametry początkowe
    x_min = 0
    x_max = size-1
    y_min = 0
    y_max = size-1

    x = 0
    y = 0

    vect_x = 0 #    0 - hold   1 - down     -1 -  up
    vect_y = 1 #    0 - hold   1 - right    -1 - left

    #algorytm
    for i in range(snake_len):

        a[x][y] = 1

        x += vect_x
        y += vect_y

        if x == x_min and y == y_max and vect_y == 1:
            vect_x = 1
            vect_y = 0
            x_min += 2
        elif x == x_max and y == y_max and vect_x == 1:
            vect_x = 0
            vect_y = -1
            y_max -= 2
        elif x == x_max and y == y_min and vect_y == -1:
            vect_x = -1
            vect_y = 0
            x_max -= 2
        elif x == x_min and y == y_min and vect_x == -1:
            vect_x = 0
            vect_y = 1
            y_min += 2

    # for j in range(size):
    #     print(a[j])

    return a


if __name__ == '__main__':

    # print(spiralize(1))
    # print(spiralize(2))
    # print(spiralize(3))
    # print(spiralize(4))
    print(spiralize(5))
    # print(spiralize(6))
    # print(spiralize(7))
    # print(spiralize(8))
    # print(spiralize(9))
    # print(spiralize(10))