
# https://www.codewars.com/kata/52e88b39ffb6ac53a400022e
def dec_to_bin(ip_dec):
    ip_bin = [0] * 32
    index = 31

    while (ip_dec):
        ip_bin[index] = ip_dec % 2
        ip_dec = ip_dec // 2
        index -= 1

    return ip_bin


def int32_to_ip(int32):
    ip_bin_arr = dec_to_bin(int32)

    sum1 = sum2 = sum3 = sum4 = 0
    base = 128

    for i in range(8):
        sum1 += base * ip_bin_arr[i]
        sum2 += base * ip_bin_arr[i + 8]
        sum3 += base * ip_bin_arr[i + 16]
        sum4 += base * ip_bin_arr[i + 24]
        base = base // 2

    ip_bin_str = f"{sum1}.{sum2}.{sum3}.{sum4}"

    return ip_bin_str


if __name__ == "__main__":
    print(int32_to_ip(2149583361))  # 128.32.10.1