
from functools import lru_cache

@lru_cache(maxsize=1000)
def fib(n):

    a, b = 0, 1
    for i in range(n):
        a, b = b, a
        a += b

    return a

if __name__ == '__main__':

    print(fib(0))
    print(fib(1))
    print(fib(2))
    print(fib(3))
    print(fib(4))
    print(fib(5))
    print(fib(1000))
    print(fib(100_000))

