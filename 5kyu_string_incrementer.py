
# https://www.codewars.com/kata/54a91a4883a7de5d7800009c
def increment_string(text):

    if text == '':
        return '1'


    length = len(text)-1
    multi = 1

    if text[length].isdecimal():
        if int(text[length]) >= 0 and int(text[length]) <= 8:
            result = text[0:length] + str(int(text[length]) + 1)
        else:
            tmp_number = 0
            only_zeros = False
            count_zeros = 0
            while length >= 0 and text[length].isdecimal() != False:
                if int(text[length]) == 0:
                    only_zeros = True
                    count_zeros += 1
                else:
                    only_zeros = False
                    count_zeros = 0

                tmp_number += int(text[length]) * multi
                multi *= 10
                length -= 1

            if len(str(tmp_number)) < len(str(tmp_number+1)):
                count_zeros -= 1

            tmp_number += 1

            result = text[0:length + 1]
            if only_zeros:
                result += '0' * count_zeros
            result += str(tmp_number)
    else:
        result = text + '1'

    return result


if __name__ == '__main__':

    print(increment_string('Tomek'))
    print(increment_string(''))
    print(increment_string('Marek00'))
    print(increment_string('Katar1113214124'))
    print(increment_string('Mydlo131248769999'))
    print(increment_string('123456789'))
    print(increment_string('Tomek00133'))
    print(increment_string('tttt099'))