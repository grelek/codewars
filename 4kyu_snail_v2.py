
# https://www.codewars.com/kata/521c2db8ddc89b9b7a0000c1
def snail(snail_map):

    if snail_map == [[]]:
        return []

    snake_len = len(snail_map) ** 2
    a = [0] * snake_len

    # parametry początkowe
    x_min = 0
    x_max = len(snail_map) - 1
    y_min = 0
    y_max = len(snail_map) - 1

    x = 0
    y = 0

    vect_x = 0  # 0 - hold   1 - down     -1 -  up
    vect_y = 1  # 0 - hold   1 - right    -1 - left

    # algorytm
    for i in range(snake_len):

        a[i] = snail_map[x][y]

        x += vect_x
        y += vect_y

        if x == x_min and y == y_max and vect_y == 1:
            vect_x = 1
            vect_y = 0
            x_min += 1
        elif x == x_max and y == y_max and vect_x == 1:
            vect_x = 0
            vect_y = -1
            y_max -= 1
        elif x == x_max and y == y_min and vect_y == -1:
            vect_x = -1
            vect_y = 0
            x_max -= 1
        elif x == x_min and y == y_min and vect_x == -1:
            vect_x = 0
            vect_y = 1
            y_min += 1

    return a