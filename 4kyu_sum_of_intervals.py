
# https://www.codewars.com/kata/52b7ed099cdc285c300001cd
def sum_of_intervals(intervals):

    memory = []
    for interval in intervals:
        a = list(interval)
        for i in range(a[0], a[1]):
            memory.append(i)

    memory = list(set(memory))
    memory.sort()

    one_group = True
    intervals_sum = 0

    i = memory[0]
    min = memory[0]
    for cell in memory:
        if cell - i > 0:
            max = i
            intervals_sum += max - min + 1
            min = cell
            i = cell
            one_group = False
        else:
            i += 1

    intervals_sum += cell - min
    if one_group:
        intervals_sum += 1

    return intervals_sum


if __name__ == '__main__':

    #print(sum_of_intervals([(1, 5)]))
    print(sum_of_intervals([(1, 5), (30, 40)]))
    print(sum_of_intervals([(1, 5), (1, 5)]))
    print(sum_of_intervals([(1, 4), (7, 10), (3, 5)]))
    print(sum_of_intervals([(1,5), (10,20), (1,6), (16,19), (5,11)]))

    # Test.assert_equals(sum_of_intervals([(1, 5)]), 4)
    # Test.assert_equals(sum_of_intervals([(1, 5), (6, 10)]), 8)
    # Test.assert_equals(sum_of_intervals([(1, 5), (1, 5)]), 4)
    # Test.assert_equals(sum_of_intervals([(1, 4), (7, 10), (3, 5)]), 7)