
# https://www.codewars.com/kata/55c45be3b2079eccff00010f
def order(sentence):

    words = sentence.split(' ')
    new_words = words.copy()

    for word in words:
        for letter in word:
            if letter >= '1' and letter <= '9':
                new_words[int(letter)-1] = word
                continue

    result = ' '
    result = result.join(new_words)

    return result


if __name__ == '__main__':

    print(order("4of Fo1r pe6ople g3ood th5e the2"))
    print(order("Wynik:" + ""))