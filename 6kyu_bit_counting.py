
# https://www.codewars.com/kata/526571aae218b8ee490006f4
def countBits(n):

    n_bin = bin(n)
    n_str = n_bin[2:]
    sum = 0

    for i in n_str:
        sum += int(i)

    return sum


if __name__ == '__main__':


    print(countBits(1234))