
# https://www.codewars.com/kata/55fd2d567d94ac3bc9000064
def row_sum_odd_numbers(n):

    start_number = n * (n-1) + 1

    sum = 0
    while n:
        sum += start_number + 2 * (n-1)
        n -= 1

    return sum


if __name__ == '__main__':

    print(row_sum_odd_numbers(1))