
# https://www.codewars.com/kata/5412509bd436bd33920011bc
def maskify(cc):

    return len(cc[:-4]) * '#' + cc[-4:]


if __name__ == '__main__':

    print(maskify('adfdsvdsdsfdscjdsjdnjsjsn'))
    print(maskify('tomek'))
    print(maskify(''))
    print(maskify('12'))
