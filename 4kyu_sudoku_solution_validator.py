
# https://www.codewars.com/kata/529bf0e9bdf7657179000008
def valid_solution(board):

    result = True

    for x in range(9):
        sum_y = 0
        for y in range(9):
            sum_y += board[x][y]
        if sum_y != 45:
            result = False
            break

    for y in range(9):
        sum_x = 0
        for x in range(9):
            sum_x += board[x][y]
        if sum_x != 45:
            result = False
            break

    tab = [[0, 2], [3, 5], [6, 8]]

    for xt in range(3):
        for yt in range(3):
            sum_mini = 0
            for x in range(tab[xt][0], tab[xt][1]+1):
                for y in range(tab[yt][0], tab[yt][1]+1):
                    sum_mini += board[x][y]
            if sum_mini != 45:
                result = False
                break

    return result


if __name__ == '__main__':

    print(valid_solution([
        [5, 3, 4, 6, 7, 8, 9, 1, 2],
        [6, 7, 2, 1, 9, 5, 3, 4, 8],
        [1, 9, 8, 3, 4, 2, 5, 6, 7],
        [8, 5, 9, 7, 6, 1, 4, 2, 3],
        [4, 2, 6, 8, 5, 3, 7, 9, 1],
        [7, 1, 3, 9, 2, 4, 8, 5, 6],
        [9, 6, 1, 5, 3, 7, 2, 8, 4],
        [2, 8, 7, 4, 1, 9, 6, 3, 5],
        [3, 4, 5, 2, 8, 6, 1, 7, 9]
    ]))

    print(valid_solution([
        [5, 3, 4, 6, 7, 8, 9, 0, 2],
        [6, 7, 2, 1, 9, 5, 3, 4, 8],
        [1, 9, 8, 3, 4, 2, 5, 6, 7],
        [8, 5, 9, 7, 6, 1, 4, 2, 3],
        [4, 2, 6, 8, 5, 3, 7, 9, 1],
        [7, 1, 3, 9, 2, 4, 8, 5, 6],
        [9, 6, 1, 5, 3, 7, 2, 8, 4],
        [2, 8, 7, 4, 1, 9, 6, 3, 5],
        [3, 4, 5, 2, 8, 6, 1, 7, 9]
    ]))