
# https://www.codewars.com/kata/521c2db8ddc89b9b7a0000c1
def snail(snail_map):

    if snail_map == [[]]:
        return []

    size_2d = len(snail_map) ** 2
    x_min = 0
    x_max = len(snail_map)-1
    y_min = 0
    y_max = x_max

    list_2d = []
    Flag = True

    while Flag:

        y = y_min
        while y <= y_max:
            list_2d.append(snail_map[x_min][y])
            y += 1

        x_min += 1
        if len(list_2d) == size_2d:
            break
        #-------------------------------------------------------------

        x = x_min
        while x <= x_max:
            list_2d.append(snail_map[x][y_max])
            x += 1

        y_max -= 1
        if len(list_2d) == size_2d:
            break
        # -------------------------------------------------------------

        y = y_max
        while y >= y_min:
            list_2d.append(snail_map[x_max][y])
            y -= 1

        x_max -= 1
        if len(list_2d) == size_2d:
            break
        # -------------------------------------------------------------

        x = x_max
        while x >= x_min:
            list_2d.append(snail_map[x][y_min])
            x -= 1

        y_min += 1
        if len(list_2d) == size_2d:
            break


    return list_2d




if __name__ == '__main__':

    array = [[1,  2,  3,  4,  5],
             [16, 17, 18, 19, 6],
             [15, 24, 25, 20, 7],
             [14, 23, 22, 21, 8],
             [13, 12, 11, 10, 9]]

    print(snail(array))

    print(snail([[]]))