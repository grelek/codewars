
# https://www.codewars.com/kata/5541f58a944b85ce6d00006a
from functools import lru_cache

@lru_cache(maxsize=100)
def fib(n):

    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)


def productFib(prod):

    n = 0
    flag = True

    while flag:
        F1 = fib(n)
        F2 = fib(n+1)

        if F1 * F2 > prod:
            flag = False
        elif F1 * F2 == prod:
            break

        n += 1

    return [F1, F2, flag]



if __name__ == '__main__':

    print(productFib(4895))
    print(productFib(5895))