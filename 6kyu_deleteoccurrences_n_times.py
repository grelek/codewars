
# https://www.codewars.com/kata/554ca54ffa7d91b236000023
def delete_nth(order, max_e):

    dict = {}
    session = []

    for num in order:
        if num not in dict:
            dict[num] = 1
        else:
            dict[num] += 1

        if dict[num] <= max_e:
            session.append(num)

    return session


if __name__ == '__main__':

    print(delete_nth([20, 37, 20, 21], 1))
    print(delete_nth([1,1,3,3,7,2,2,2,2], 3))