
# https://www.codewars.com/kata/53dbd5315a3c69eed20002dd
def filter_list(l):
    'return a new list with the strings filtered out'

    new_l = []

    for x in l:
        if isinstance(x, int):
            new_l.append(x)

    return new_l


if __name__ == '__main__':

    c = [1, 2, 3, 'd']

    print(filter_list(c))

