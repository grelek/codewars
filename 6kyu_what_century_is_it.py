
# https://www.codewars.com/kata/52fb87703c1351ebd200081f
def century(year):

    cent = int(year / 100)
    if year % 100 != 0:
        cent += 1

    return cent

def what_century(year):

    century_str = str(century(int(year)))
    ends = ['st', 'nd', 'rd', 'th']
    n = int(century_str[len(century_str)-1])
    m = int(century_str[len(century_str)-2])

    if n > 3 or m == 1:
        century_str += ends[3]
    else:
        century_str += ends[n-1]

    return century_str

if __name__ == '__main__':

    test = 1223
    print(century(test))
    print(what_century(str(test)))