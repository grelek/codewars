
# https://www.codewars.com/kata/585d7d5adb20cf33cb000235
from collections import Counter

def find_uniq(arr):

    a = Counter(arr).most_common()
    b = list(a)

    return b[-1][0]


if __name__ == '__main__':

    print(find_uniq([1, 2, 3, 1, 2, 3, 4, 1, 2, 3]))