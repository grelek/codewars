
# https://www.codewars.com/kata/5656b6906de340bd1b0000ac
def longest(s1, s2):

    from collections import Counter

    c = sorted(list(Counter(s1 + s2)))

    string = ''
    for l in c:
        string += l

    return string


if __name__ == '__main__':

    print(longest('tomek', 'kinga'))
