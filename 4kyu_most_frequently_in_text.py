
# https://www.codewars.com/kata/51e056fe544cf36c410000fb
from collections import Counter
import re

def top_3_words(text):

    text = text.lower()
    text = text.replace('/', ' ')
    text = text.replace(':', ' ')
    text = text.replace(';', ' ')
    text = text.replace('!', ' ')
    text = text.replace('?', ' ')
    text = text.replace('-', ' ')
    text = text.replace('_', ' ')
    text = text.replace(',', ' ')
    text = text.replace('.', ' ')
    tab = text.split(' ')
    dict = {}

    for word in tab:
        a = re.findall("[a-z]+[a-z']*", word)
        if a:
            if word in dict:
                dict[word] += 1
            else:
                dict[word] = 1

    counter = Counter(dict).most_common()
    list = []
    i = 0
    for w in counter:
        list.append(w[0])
        i += 1
        if i >= 3:
            break

    return list

if __name__ == '__main__':

    print(top_3_words('.... ... sdsad asd sa\'das da sdas//d a'))
    print(top_3_words("""In a village of La Mancha, the name of which I have no desire to call to
    mind, there lived not long since one of those gentlemen that keep a lance
    in the lance-rack, an old buckler, a lean hack, and a greyhound for
    coursing. An olla of rather more beef than mutton, a salad on most
    nights, scraps on Saturdays, lentils on Fridays, and a pigeon or so extra
    on Sundays, made away with three-quarters of his income."""))