
# https://www.codewars.com/kata/5552101f47fc5178b1000050
def dig_pow(n, p):

    n_str = str(n)
    sum = 0

    for number in n_str:
        sum += int(number) ** p
        p += 1

    k = sum/n

    if int(k) != 0 and k % int(k) == 0:
        result = int(k)
    else:
        result = -1

    return result


if __name__ == '__main__':

    print(dig_pow(92, 1))
