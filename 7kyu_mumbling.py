
# https://www.codewars.com/kata/5667e8f4e3f572a8f2000039
def accum(s):

    word = s.upper()

    result = word[0]
    i = 1
    for letter in word[1:]:
        result += '-' + letter + letter.lower() * i
        i += 1
        print(letter)

    return result


if __name__ == '__main__':

    print(accum('tOmEK'))