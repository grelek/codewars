
# https://www.codewars.com/kata/5a3fe3dde1ce0e8ed6000097
def century(year):

    cent = int(year / 100)
    if year % 100 != 0:
        cent += 1

    return cent


if __name__ == '__main__':

    print(century(2505))