
# https://www.codewars.com/kata/578553c3a1b8d5c40300037c
def binary_array_to_number(arr):

    result = 0
    n = len(arr) - 1
    for b in arr:
        result += b * (2 ** n)
        n -= 1

    return result


if __name__ == '__main__':

    print(binary_array_to_number([0, 1, 0, 1]))