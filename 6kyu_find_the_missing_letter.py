
# https://www.codewars.com/kata/5839edaa6754d6fec10000a2
import string

def find_missing_letter(chars):

    small = chars[0].islower()
    if small:
        alphabet = '-'.join(list(string.ascii_lowercase))
    else:
        alphabet = '-'.join(list(string.ascii_uppercase))


    st_l = alphabet.find(chars[0])
    nd_l = alphabet.find(chars[len(chars)-1])

    part_of_alphabet = alphabet[st_l : nd_l+1]

    result = set(part_of_alphabet.split('-')) - set(chars)
    result = ''.join(result)


    return result



if __name__ == '__main__':

    print(find_missing_letter(['O','Q','R','S']))